function deleteOtherCookies(){
	let cookies = document.cookie.split(';');
	for(let i = 0; i < cookies.length; i++){
		let item = cookies[i];
		if(item[0] === ' '){
			item = cookies[i].slice(1);
		}
		if(!item.startsWith('sessionId')){
			let pos = cookies[i].indexOf('=');
			let name = cookies[i].slice(0, pos);
			deleteCookie(name);
		}
	}
}

function deleteCookie(name){
	let cookieDate = new Date();
	cookieDate.setSeconds(cookieDate.getSeconds() - 1000);
	let date = cookieDate.toUTCString();
	document.cookie = name +
		'=; expires=' + date + ';';
}

function createCookie(key, data){
	let name = 'system' + data.systemId;
	let cookieDate = new Date();
	cookieDate.setDate(cookieDate.getFullYear() + 1);
	document.cookie = 
		key +
		'=' + data +
		'; expires=' +
		cookieDate.toUTCString();
}
