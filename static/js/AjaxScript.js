function getData(url, timeOut, successFunc){

	$.ajax({
		type: 'GET',
		url: url,
		dataType: 'text',
		cache: false,
		timeout: timeOut,
		success: function(data){
			currentStateServer = true;
			messageSwitch(false, '');
			console.log(data);
			if(data !== ""){
				successFunc(data);
			}
		},
		error: function(){
			currentStateServer = false;
			errorMessageFunc();
		}
	});
}

function postData(url, timeOut, data, successFunc, errorFunc){

	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		dataType: 'text',
		cache: false,
		timeout: timeOut,
		success: function(data){
			currentStateServer = true;
			messageSwitch(false, '');
			console.log(data);
			if(data !== ""){
				successFunc(data);
			}
		},
		error: function(){
			currentStateServer = false;
			errorFunc();
		}
	});
}
