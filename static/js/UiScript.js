// UI Script file
// add non-touch class if browser doesn't support touch
var touchsupport = ('ontouchstart' in window) || (navigator.maxTouchPoints > 0) || (navigator.msMaxTouchPoints > 0)

if (touchsupport){
	let x = document.getElementsByClassName("w3-bar-item");
	for(let i = 0; i < x.length; i++){
		x[i].className += " w3-hover-white"
	}
}

// Toggle between showing and hiding the sidebar when clicking the menu icon
var sideBar = document.getElementById("sideBar");

function SideBarOpen() {
	if (sideBar.style.display === 'block') {
		sideBar.style.display = 'none';
	} else {
		sideBar.style.display = 'block';
	}
}

// Close the sidebar with the close button
function SideBarClose() {
	sideBar.style.display = "none";
}
