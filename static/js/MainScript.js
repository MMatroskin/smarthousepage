function getCookieValue(name){
	let cookie = document.cookie;
	let value = getValue(cookie, name, '=',';');
	if(value.startsWith('"')){
		value = value.slice(1,);
	}
	if(value.endsWith('"')){
		value = value.slice(0,value.length - 1);
	}
	return value;
}

function getValue(str, key, sepBegin, sepEnd){
	let result = '';
	let pos = str.indexOf(key);
	if (pos !== -1){
		let posBegin = str.indexOf(sepBegin, pos);
		let posEnd = str.indexOf(sepEnd, pos);
		if (posEnd !== -1 && posBegin !== -1){
			result = str.slice(posBegin + sepBegin.length, posEnd);
		}else if(posBegin !== -1){
			result = str.slice(posBegin + 1);
		}
	}
	return result;
}

function getVarFromString(str, key){
	return  getValue(str, key, ':', ' ');
}

function getPositiveInt(values, key, defaultVal){
    let result = parseInt(getVarFromString(values, key));
    if(isNaN(result) || result <= 0){
        result = defaultVal;
    }
    return result;
}

function isEmptyObject(object){
	return JSON.stringify(object) == '{}';
}

function setMessageText(msg){
    let msgText = document.getElementById("messageText");
    if(msgText !== null){
        msgText.innerHTML = msg;
    }
}

function messageSwitch(show, msg) {
	let message = document.getElementById("message");
	if(show === false) {
		message.style.display = 'none';
	}else{
		setMessageText(msg);
		message.style.display = 'block';
	}
}

function errorMessageFunc(){
	console.log('Server not response');
	messageSwitch(true, 'Сервер не отвечает');
}

var initDataCookie = getCookieValue('initData');
var isActive = true;
var updateRuning = true;
var currentStateServer = true;
var countSystem = getPositiveInt(initDataCookie, 'countSystem', 2);
var timeOutMin = getPositiveInt(initDataCookie, 'timeOutMin', 500);
var timeOutMax = getPositiveInt(initDataCookie, 'timeOutMax', 5000);
var interval = getPositiveInt(initDataCookie, 'interval', 30000);
var switchOnZero = getPositiveInt(initDataCookie, 'switchOnZero', 0);
var savedSlidersState = new Array(countSystem);

for(let i = 0; i < countSystem; i++){
	savedSlidersState[i] = 50;
}
document.getElementById("timeOut").innerHTML =  interval / 1000;

window.onfocus = function(){
	isActive = true;
	if(updateRuning === false){
		updateRuning = true;
		getDataOnTimer();
	}
};

window.onblur = function(){
	isActive = false;
};

document.addEventListener('DOMContentLoaded', function(){
	getDataOnTimer();
});
