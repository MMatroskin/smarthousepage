function setDataMainValues(data){
	data.forEach(function(item){
		let element = document.getElementById(item.id);
		if (element !== null) {
			element.innerHTML = item.value;
		}
	});
}

function splitAndSetDataValues(data){
	let jsonData = JSON.parse(data);
	let dataMain = [];
	let dataLum = [];
	jsonData.forEach(function(item){
		if(typeof item.systemId === 'undefined'){
			dataMain.push(item);
		} else{
			dataLum.push(item);
		}
	});
	setDataMainValues(dataMain);
	setDataLightValues(dataLum);
}

function getDataOnTimer() {
	if(updateRuning){
		if (isActive) {
			let timeOut = interval / 2;
			if (timeOut > timeOutMax) {
				timeOut = timeOutMax;
			}
			let url = 'ajax/refreshdata/';
			getData(url, timeOut, splitAndSetDataValues);
			setTimeout(getDataOnTimer, interval);
		}else{
			updateRuning = false;
		}
	}
}
