function getElementsId(systemId = 0){
	let elementsId = {};
	elementsId.sliderId = 'slider' + systemId;
	elementsId.stateLabelId = 'sliderLabel' + systemId;
	elementsId.switcherId = 'switcher' + systemId;
	return elementsId;
}

function setSliderLabelValue(stateLabelId, value){
	let stateLabel = document.getElementById(stateLabelId);
	if(stateLabel !== null){
		stateLabel.innerHTML = value;
	}
}

function setDataLightValues(data){
	data.forEach(function(value){
		let elementsId = getElementsId(value.systemId);
		let slider = document.getElementById(elementsId.sliderId);
		if(slider !== null){
			let newSliderValue = Math.round(value.sliderState);
			slider.value = newSliderValue;
			setSliderLabelValue(elementsId.stateLabelId, newSliderValue);
			savedSlidersState[value.systemId] = newSliderValue;
		}
		setSwitcher(elementsId.switcherId, value.switcherState);
	});
}

function setSwitcher(id, switcherState=0){
	let switcher = document.getElementById(id);
	if(switcher !== null){
		let value = 'Вкл.';
		let title = 'on';
		if(switcherState !== 0){
			value = 'Выкл.';
			title = 'off';
		} // display negative values !!!
		switcher.title = title;
		switcher.innerHTML = value;
	}
}

function onSliderValueChanged(systemId){
	let elementsId = getElementsId(systemId);
	let sliderValue = parseInt(document.getElementById(elementsId.sliderId).value);
	setSliderLabelValue(elementsId.stateLabelId, sliderValue);
	let switcherTitle = document.getElementById(elementsId.switcherId).title;
	if(switcherTitle === 'off'){
		let newSwitcherState = 1; // new state for switcher state on server (negative!!)
		if(switchOnZero !== 0 && sliderValue === 0){
			newSwitcherState = 0;
		}
		let data = createData(systemId, newSwitcherState, sliderValue);
		postLightItemData(data);
	}
}

function onSwitcherClicked(systemId){
	let elementsId = getElementsId(systemId);
	let sliderValue = parseInt(document.getElementById(elementsId.sliderId).value);
	let switcherTitle = document.getElementById(elementsId.switcherId).title;
	let newSwitcherState = 0;
	if(switcherTitle === 'on'){
		newSwitcherState = 1;
	}
	let data = createData(systemId, newSwitcherState, sliderValue);
	postLightItemData(data);
}

function createData(systemId, switcherState, sliderValue){
	let json = {
		"systemId": systemId,
		"switcherState": switcherState,
		"sliderState": sliderValue
	};
	return  JSON.stringify(json);
}

function setDataLights(data){
	let jsonData  = JSON.parse(data);
	let elementsId = getElementsId(jsonData.systemId);
	if(jsonData.success === 0){
		// error on controller
		let slider = document.getElementById(elementsId.sliderId);
		if(slider !== null){
			slider.value = jsonData.sliderState;
			setSliderLabelValue(elementsId.stateLabelId, jsonData.sliderState);
		}
	}
	setSwitcher(elementsId.switcherId, jsonData.switcherState);
	savedSlidersState[jsonData.systemId] = jsonData.sliderState;
}

function ifPostDataError(){
	for(let id = 0; id < countSystem; id++){
		let elementsId = getElementsId(id);
		let slider = document.getElementById(elementsId.sliderId);
		if(slider !== null){
			slider.value = savedSlidersState[id];
			setSliderLabelValue(elementsId.stateLabelId, savedSlidersState[id]);
		}
	}
	errorMessageFunc();
}

function postLightItemData(data){
	let timeOut = timeOutMin;
	let url = 'ajax/updatelum/';
	postData(url, timeOut, data, setDataLights, ifPostDataError);
}
